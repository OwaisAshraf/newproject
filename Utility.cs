﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using MySql.Data.MySqlClient;
using NPoco;
//using Survey.DataAccess.Repositories;
using System;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;

namespace Survey.DataAccess
{
    public class Utility
    {
        public static IConfiguration Config => LoadConfig();
        public static IConfigurationRoot LoadConfig()
        {
            return new ConfigurationBuilder()
                .SetBasePath(System.AppContext.BaseDirectory)
                .AddJsonFile("appsettings.json").Build();
        }

        public Utility()
        {
            LoadConfig();
        }


        public static class ResponseMessage
        {
            public static string Ok = "Successfull";
            public static string Already = "Already registered";
            public static string Unauthorized = "401";
            public static string BadRequest = "Bad Request";
            public static string NotFound = "Not Found";
            public static string InternalServerError = "Internal server error";
            public static string Delete = "Your data has been successfully deleted!";
            public static string SurveyNotFound = "Survey Not Found";
            public static string Blocked => "Your account is blocked";
            public static string EmailNotVerified => "Please verify your email before creating a survey";
            public static string PhoneNotVerified => "Please verify your mobile number before creating a survey";
        }
        public static string Earned = "Earned";

        public const string ACCOUNT_SID = "AC7b25a52d9cb60da6cd33086b78626e3b";
        public const string ACCOUNT_TOKEN = "2008643431b214cfe27060c08885339b";
        public const string TWILIO_NUMBER = "+17154686336";

        //public static string GetBaseUrl()
        //{
        //    if (MyHttpContext.Current == null || MyHttpContext.Current.Request == null) return "";
        //    var request = MyHttpContext.Current?.Request;
        //    var host = request?.Host.ToUriComponent();
        //    var pathBase = request?.PathBase.ToUriComponent();
        //    return $"{request?.Scheme}://{host}{pathBase}" + "/";
        //}


        public static string Encrypt(string text)
        {
            var encodedPassword = new UTF8Encoding().GetBytes(text);
            var hash = ((HashAlgorithm) CryptoConfig.CreateFromName("MD5")).ComputeHash(encodedPassword);
            var encoded = BitConverter.ToString(hash)
                .Replace("-", string.Empty)
                .ToLower();
            return encoded;
        }

        //public static class Instances
        //{
        //    private static CardRepo _cardRepo = null;
        //    public static CardRepo CardRepo => _cardRepo == null ? new CardRepo() : _cardRepo;
        //}

        public enum CardType
        {
            MasterCard, Visa, AmericanExpress, Discover, JCB,
            Unknown
        };

        public static CardType FindType(string cardNumber)
        {
            //https://www.regular-expressions.info/creditcard.html
            if (Regex.Match(cardNumber, @"^4[0-9]{12}(?:[0-9]{3})?$").Success)
            {
                return CardType.Visa;
            }

            if (Regex.Match(cardNumber, @"^(?:5[1-5][0-9]{2}|222[1-9]|22[3-9][0-9]|2[3-6][0-9]{2}|27[01][0-9]|2720)[0-9]{12}$").Success)
            {
                return CardType.MasterCard;
            }

            if (Regex.Match(cardNumber, @"^3[47][0-9]{13}$").Success)
            {
                return CardType.AmericanExpress;
            }

            if (Regex.Match(cardNumber, @"^6(?:011|5[0-9]{2})[0-9]{12}$").Success)
            {
                return CardType.Discover;
            }

            if (Regex.Match(cardNumber, @"^(?:2131|1800|35\d{3})\d{11}$").Success)
            {
                return CardType.JCB;
            }

            return CardType.Unknown;
        }

        private static string ConnectionString =>
           Config.GetSection("ConnectionStrings").GetSection("Default").Value;

        public static IDatabase Database =>
            new ProfiledDatabase(ConnectionString, DatabaseType.MySQL, MySqlClientFactory.Instance);
        public class AppConfig
        {
            public string GetValue(string section, string defaultValue)
            {
                var sec = Config.GetSection(section);
                if (sec == null) return defaultValue;
                return sec.Value == null ? defaultValue : sec.Value;
            }

            public string GetValue(string section, string subSection, string defaultValue)
            {
                var sec = Config.GetSection(section);
                if (sec == null) return defaultValue;
                var subSec = sec.GetSection(subSection);
                if (subSec == null) return defaultValue;
                return subSec.Value == null ? defaultValue : subSec.Value;
            }
        }


    }
}